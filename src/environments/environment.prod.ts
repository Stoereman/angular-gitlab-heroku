const packagejson = require('../../package.json')

export const environment = {
  production: true,

  // Fill in your own online server API url here
  apiUrl: 'https://clientsidedm2021.herokuapp.com/api',

  version: packagejson.version
}

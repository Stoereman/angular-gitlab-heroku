import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../user/user/user.model';
import { UserService } from '../user/user/user.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styles: [
  ]
})
export class UserDetailComponent implements OnInit {
  //Declareren in constructor, gebruiken in init
  userId: string|null=null;
  user: User | null = null;
  constructor(
    private route: ActivatedRoute,
    private userService: UserService
    ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
    this.userId= params.get('userId')
    this.user = this.userService.getUserById(Number(this.userId));
  })
  }

}

import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { User, UserRole } from './user.model';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  readonly users: User[] = [
    {
      id: 0,
      firstName: 'First',
      lastName: 'User',
      emailAdress: 'userOne@user.com',
      role: UserRole.admin,
    },
    {
      id: 1,
      firstName: 'Second',
      lastName: 'User',
      emailAdress: 'userTwo@user.com',
      role: UserRole.guest,
    },
    {
      id: 2,
      firstName: 'Third',
      lastName: 'User',
      emailAdress: 'userThree@user.com',
      role: UserRole.editor,
    },
    {
      id: 3,
      firstName: 'Fourth',
      lastName: 'User',
      emailAdress: 'userFour@user.com',
      role: UserRole.editor,
    },
    {
      id: 4,
      firstName: 'Fifth',
      lastName: 'User',
      emailAdress: 'userFive@user.com',
      role: UserRole.editor,
    },
  ];

  constructor() {
    console.log('Service constructor called');
  }

  getUsers(): User[] {
    console.log('getUsers called');
    return this.users;
  }

  getUsersAsObservable(): Observable<User[]> {
    console.log('getUsersAsObservable called');
    return of(this.users);
  }

  getUserById(id: number): User {
    console.log('getUserById called');
    return this.users.filter((user) => user.id === id)[0];
  }
}

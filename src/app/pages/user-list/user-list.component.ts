import { Component, OnInit } from '@angular/core';
import { UserService } from '../user/user/user.service';
import { User } from '../user/user/user.model';

@Component({
  selector: 'app-list',
  templateUrl: './user-list.component.html',
  styles: [
  ]
})
export class ListComponent implements OnInit {
  users: User[] = [];
  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.users = this.userService.getUsers();
  }

}

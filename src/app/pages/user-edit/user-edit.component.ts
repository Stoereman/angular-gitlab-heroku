import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../user/user/user.model';
import { UserService } from '../user/user/user.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styles: [
  ]
})
export class UserEditComponent implements OnInit {
  userId: string | null = null;
  user: User | null = null;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.userId = params.get('userId');
      if (this.userId) {
        this.user = this.userService.getUserById(Number(this.userId));
      } else {
        this.user = new User();
      }
    });

  }
  save(){
    console.log('Future action TBA');
    this.router.navigate(['..'], { relativeTo: this.route });

  }

}

import { Component, OnInit } from '@angular/core';
import { UseCase } from './usecase/usecase.model';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styles: [
  ]
})
export class AboutComponent implements OnInit {
  readonly PLAIN_USER = 'Regular user'
  readonly ADMIN_USER = 'Administrator'

  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Login',
      description: 'This is how an existing user logs in.',
      scenario: [
        'User enters credentials and hits the login button.',
        'The application processes the request.',
        'If correct, the user is redirected to the home page.'
      ],
      actor: this.PLAIN_USER,
      precondition: 'None',
      postcondition: 'The user has been logged in.'
    },
    {
      id: 'UC-02',
      name: 'Create a Product',
      description: 'This is how an existing user creates a Product.',
      scenario: [
        'The User clicks the "new" button.',
        'A form appears, the user enters the desired information.',
        'The user clicks submit.',
        'The system validates if the information is formatted correctly.',
        'If correct, the user receives a success notification',
        'The user is redirected to the list view accompanied by the product details.'
      ],
      actor: this.PLAIN_USER,
      precondition: 'The user has been logged in',
      postcondition: 'A Product has been created.'
    }
  ]


  constructor() { }

  ngOnInit(): void {
  }

}

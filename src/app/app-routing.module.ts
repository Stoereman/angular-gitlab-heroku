import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListComponent } from './pages/user-list/user-list.component';
import { ProductComponent } from './pages/product/product.component';
import { UserComponent } from './pages/user/user.component';
import { UserDetailComponent } from './pages/user-detail/user-detail.component';
import { ColumnsComponent } from './pages/columns/columns.component';
import { UserEditComponent } from './pages/user-edit/user-edit.component';
import { AboutComponent } from './pages/about/about.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'about'},
  {path: 'user', pathMatch: 'full' , component: UserComponent},
  {path: 'product', pathMatch: 'full' , component: ProductComponent},
  {path: 'userlist', pathMatch: 'full' , component: ListComponent},
  {path: 'userlist/:userId', pathMatch: 'full', component: UserDetailComponent},
  {path: 'columns', component: ColumnsComponent, children: [
    {path: ':userId', pathMatch: 'full', component: UserDetailComponent},
    {path: 'new', pathMatch: 'full', component: UserEditComponent},
    {path: ':userId/edit', pathMatch: 'full', component: UserEditComponent}]},
  {path: 'about', pathMatch: 'full', component: AboutComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
